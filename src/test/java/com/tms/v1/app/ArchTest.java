package com.tms.v1.app;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.tms.v1.app");

        noClasses()
            .that()
            .resideInAnyPackage("com.tms.v1.app.service..")
            .or()
            .resideInAnyPackage("com.tms.v1.app.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..com.tms.v1.app.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}

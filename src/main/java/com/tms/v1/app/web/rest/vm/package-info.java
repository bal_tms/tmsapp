/**
 * View Models used by Spring MVC REST controllers.
 */
package com.tms.v1.app.web.rest.vm;
